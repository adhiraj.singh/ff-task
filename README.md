# FF Task:

Train a model on the FairFace dataset ([found here](https://github.com/joojs/fairface)) for the following tasks:
- Gender Classification (visually apparent gender, no need to get into the depth of what is gender)
- Age prediction.
- Race classification, use the broader set of race labels, not sub-race classes.

You're to train **ONE model** for all the 3 tasks, you can take a multi-task approach, use a pre-trained backbone and do transfer-learning with only the FC layers being added, you can do regression/band classification for age etc.

This is not a competition task, so absolute results don't have a lot of weights, focus for this one is how you approach the Project life cycle:
- How to do you brainstorming and literature surveys.
- How you set up your code repos, do collaboration on the tasks and code collab over git.
- How you track and log the experiments,
- How you present the results, and how ready your code and model is if it were to be taken to prod.


## Collaborators: 

- Kartik 
- Suhas 
- Adhiraj


## Task To Dos:

- [x] Add members.
- [x] Create template for readme file.
- [x] Add literature survey docs.
- [x] Brainstorming meeting.
- [x] Setup systems (VS Code, WandB).
- [x] Create Branches.
- [X] Upload to Branches.
- [ ] Finish documentation.
- [ ] Merge Branches.

## About the dataset:
- The Fair Face dataset has 86744 images for training and 10954 images for validation.
- It has 9 age categories: **'50-59', '30-39', '3-9', '20-29', '40-49', '10-19', '60-69', '0-2', 'more than 70'.** 
- 8 race categories: **'East Asian', 'Indian', 'Black', 'White', 'Middle Eastern', 'Latino_Hispanic', 'Southeast Asian'**
- 2 gender categories: **'Male', 'Female'**

![](Images/img4.png)      

Age Categories: {0:"0-2", 1:"3-9", 2:"10-19", 3:"20-29", 4:"30-39", 5:"40-49", 6:"50-59", 7:"60-69", 8:"more than 70"}

![](Images/img5.png)


## Ideation and Literature Survey:

1. **Kimmo K., Jungseock J.FairFace: Face Attribute Dataset for Balanced Race, Gender, and Age for Bias Measurement and Mitigation, WACV 2021**

   - Baseline dataset paper (Solution to the existing bias problem of gender and race classification solutions) (Source: Flickr, Twitter, Newspapers, Web)
   - Well balanced dataset among 7 races and 108k images. 

   ![Fig 1. Image ratio for different races](Images/img1.png "Image ratio for different races")
   Fig 1. Image ratio for different races

   - Used ResNet-34 architecture with ADAM optimizer. 
   - Compared the results with UTKFace, LFWA+, and CelebA datasets
   - Performed cross dataset testing.  

   ![](Images/img2.png)


2. **Ke Zhang., et. al, Fine-Grained Age Estimation in the Wild with
Attention LSTM Networks. [code](https://github.com/PRIS-CV/Fine-Grained-Age-Estimation-in-the-Wild-With-Attention-LSTM-Networks) IEEE Trans. Cir. and Sys. for Video Technol. 30, 9 (Sept. 2020)**

   - Authors trained AL-ResNets (AL- Attention long-short term) to extract local age sensitive regions. 
   - Used Adience dataset for age-group classification.

   ![Fig: Model training pipeline](Images/img3.png "Model training pipeline")
   Fig: Model training pipeline

## Experimentation:

### Test 1.1: CNN Baseline (Reduced Dataset):

   - Model Converged and overfit on training data with 3200 training samples.
   
### Test 1.2: Res34 (Pretrained ImageNet) Without weighting loss:

   - Accuracy
   - ------------------------------
   - Age:    | 99.633  |  52.568 |
   - Gender: | 99.916  |  92.27  |
   - Race:   | 99.755  |  65.844 |
   - ------------------------------

   ![](Images/charts.png) 


### Test 1.3: Res34 (Pretrained ImageNet) With weighting loss:

   - Accuracy
   - -----------------------------
   - Age:    | 99.638  |  53.262 |
   - Gender: | 99.856  |  91.685 |
   - Race:   | 99.69   |  64.675 |
   - -----------------------------

   ![](Images/charts1.png) 





### Test 2: Res34(Head cut off + 3 Task Heads:
   - **Stage 1:** BB frozen, Trained only the new 3 heads, LR1e-2, 15 Epochs. **L_All = L_age + L_gen + L_race**
   - Final Acc: **Age: 7.3%, Gender: 79.8065%, Race: 48.7037%**

   ![](Images/img6.png)

   - **Stage 2:** Trained BB + heads, LR min 1e-6 max 3e-4, 100 Epochs.
   - Final Acc: **Age: 7.5771%, Gender: 84.3436%, Race: 57.1937%**

   ![](Images/img7.png)



